# Development flow
 - setup development environment
 - describe environment in `sh_ver.h.in`
 - make mods
 - `cmake --build .`
 - test everything
 - `gca`
 - `bumpversion patch`
 - `gpoat` instead of `gpp`
