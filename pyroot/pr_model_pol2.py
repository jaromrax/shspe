
# ------------------------- MINUIT PART ----------------
#  pip3 install iminout  numba_stats numpy
from iminuit import cost, Minuit
import iminuit
import ROOT
import numpy as np
from numba_stats import norm, uniform # faster replacements for scipy.stats functions


def main(x,y,dy):
    # --++++++++++++++++++++++++++++------------chi2
    def model_chi2(x,   a,b,c):
        f = a* x**2 + b*x + c
        return f

    # ---- for histograms, use cx...
    c2 = cost.LeastSquares(x, y, dy, model_chi2, loss='soft_l1')
    m2 = Minuit(c2,
                a=0.01, b=0.1, c=0.1)

    m2.migrad()       # DO MINIMIZATION <<<<<<<<<<
    chi_red = m2.fval/(len(x) - m2.nfit)
    return chi_red, m2.values
