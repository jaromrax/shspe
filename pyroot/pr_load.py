#!/usr/bin/env python3
# MODIFIED FILE TO READ THE DATA FROM FILES FROM MAIL
import ROOT
import numpy as np
from array import array
import random
import os
import pandas as pd

from fire import Fire

import prun # looks like mutual import, but it is not really

#   the DAT file can contain TAGS:
#   #COLNAME: frame,x,y     ... column names (selfexplaining)
#   #LOAD_AS: x,dx,y,dy     ... order of columns  for x,y,dx,dy TGraph(Errors)
#



def main( *args ):

    print(f"i... @load: args={args}")
    if len(args)==0:
        print("X... no file given")
        return

    fname = args[0]
    if not os.path.exists( fname ):
        print(f"X... data file {fname} not found")
        return

    # - column names --------
    names = ['x', 'y', 'dy']
    xynames = ['x', 'y', 'dy']

    print(names, xynames)

    # count columns
    df = pd.read_csv(fname, delimiter=" ", header=None, comment="#", nrows=2)
    i=1
    while len(names) < df.shape[1]:
        names.append(f"col{i:d}")
        i+=1

    # -read all data now; column names are from COLNAMES or xynames(if not def)
    df = pd.read_csv(fname, delimiter="\t", header=None, comment="#", names=names, decimal=",")
    print(df)

    # ------ graph name in CLING
    NAME = os.path.splitext(fname)[0]
    xpos = xynames.index('x')
    ypos = xynames.index('y')
    dypos = xynames.index('dy')
    x = np.array(df[df.columns[xpos]],  np.float64)
    y = np.array(df[df.columns[ypos]],  np.float64)
    dy = np.multiply(y, (np.array(df[df.columns[dypos]],  np.float64)/100))
    dx = np.zeros_like(x)

    #c1 = ROOT.TCanvas('c1', '', 200, 10, 700, 500)
    g = ROOT.TGraphErrors(len(x), x.flatten("C"), y.flatten("C"), dx.flatten("C"), dy.flatten("C") )

    #c1.Update()
    #c1.Print()
    # g.Print()
    # plot nicely on (maybe new) TCanvas
    g.SetMarkerStyle(7) # small circle , no lines...
    g.SetMarkerStyle(1) # small circle , no lines...
    g.Draw("PAW")
    ROOT.gPad.SetGrid()
    #c1.SetLogy()
    #c1.Update()
    #c1.SetTitle(f"{NAME};{names[xpos]};{names[ypos]}")
    prun.register(g,NAME)

    # # make it available from CLING
    # # 1st remove all with the same name
    # rmo = ROOT.gDirectory.Get(NAME)
    # while rmo!=None:
    #     print(f"x... removing previous object named {NAME}")
    #     ROOT.gDirectory.Remove(rmo)
    #     rmo = ROOT.gDirectory.Get(NAME)

    # # make it available from CLING
    # g.SetName(NAME)
    # g.SetTitle(NAME)

    # # --------------- here is a problem. It doesn persist neither in gDir nor in GetListOfSpecials
    # # --------------- but the name remains when asked from CLING  !?
    # print("")
    # oldg = ROOT.gROOT.GetListOfSpecials().FindObject(NAME)
    # if oldg:
    #     ROOT.gROOT.GetListOfSpecials().Remove(oldg)
    # ROOT.gROOT.GetListOfSpecials().Add(g)
    # ROOT.gROOT.GetListOfSpecials().ls()
    # ROOT.gDirectory.Add(g)


    # print(f"i... ls of gDirectory  {NAME}:")
    # ROOT.gDirectory.ls()
    # print("")
    # print("_______")
    # print(f"  {NAME} ")
    # print("_______")

    return

if __name__=="__main__":
    Fire(main)
    input('press ENTER to end...')
