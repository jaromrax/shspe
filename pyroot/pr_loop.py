#!/usr/bin/env python3

import ROOT
import numpy as np
from array import array
import random
import os
import pandas as pd
import time

from fire import Fire
import importlib # dynamic import

import sys # to unload

import prun   # this seems like mutual import, but it is not really

# ------------------------- MINUIT PART ----------------
#  pip3 install iminout  numba_stats numpy
from iminuit import cost, Minuit
import iminuit
from numba_stats import norm, uniform # faster replacements for scipy.stats functions


def main(*args):
    # make new root file with new tree
    file = open('results.txt', 'w')
    file.write("En\t f1: p1\t p2 \t p3\t f2:p1\t t2\n")
    file.close()
    for i in range(int(args[0]), int(args[1])):
        fname = f"G{i}.txt"
        module_fit = importlib.import_module("pr_fit")
        module_fit.main(fname, 'pol2', 'pol1', 'x,y,dy', 'results.txt')
    df = pd.DataFrame(columns=['En', 'p11', 'p12', 'p13', 'p21', 'p22'])
    with open('results.txt', 'r') as data_file:
        data_file.seek(0)
        lines = data_file.readlines()

        for i in range(1, len(lines)):
            df.loc[len(df)] = np.asarray(lines[i].split(), dtype='d').flatten("C")
    print(df)

    c = ROOT.gPad.GetCanvas()  # reset all canvas
    c.Clear()

    multi = ROOT.TMultiGraph()

    first_formula = ROOT.TFormula("f1", 'exp([0]*log(x)*log(x)+[1]*log(x)+[2])')
    second_formula = ROOT.TFormula("f2", 'exp([3]*log(x)+[4])')

    first_func = ROOT.TF1('pol2', 'f1', 0, 3000)
    second_func = ROOT.TF1('pol1', 'f2', 0, 3000)
    gr_l = []
    for i in range(0, len(df)):
        gr_l.append(ROOT.TGraph())
        first_func.SetParameters(df['p11'][i], df['p12'][i], df['p13'][i])
        second_func.SetParameters(df['p21'][i], df['p22'][i])

        for j in range(0, int(df['En'][i])):
            value = first_func.Eval(j)
            gr_l[i].SetPoint(j, j, value)
        for j in range(int(df['En'][i]), 3000):
            value = second_func.Eval(j)
            gr_l[i].SetPoint(j, j, value)
        multi.Add(gr_l[i])
        gr_l[i].SetTitle(f"G{int(args[0]) + i}")
    multi.Draw("a fb l3d")
    prun.register(multi, "multi")


if __name__=="__main__":
    Fire(main)
    # update canvas
    ROOT.gSystem.ProcessEvents()
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    ROOT.gPad
    input('press ENTER to end...')