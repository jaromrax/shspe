#!/usr/bin/env python3

import prun
import ROOT
import time

# loading histogram
prun.loadpy("load","cugamma_cu4.txt _,h")

# zooming
prun.loadpy("zoom","cugamma_cu4 7432,50")

# fitting
res = prun.loadpy("fit","cugamma_cu4 gpol1")  #print(res.keys() )


if ( type(res) is dict) and ( res['noerror']):
    print(f"@ {res['channel']:.2f} A = {res['area']:.2f} {res['darea']:.2f}")
    print(res['diff_fit_int_proc'],"%" )
else:
    print("X... problem in fit OR data not returned in dict")



# wait closing
while ROOT.addressof(ROOT.gPad)!=0: time.sleep(0.2)
