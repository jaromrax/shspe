import ROOT
import numpy as np
from array import array
import random


print("i... I am in pr_fitpol.py")
# ------ graph name in CLING
NAME = "g"

# ------ statistics
sigma = 1
chi = 1

# x = np.array(  [0,1,2,3,4,5,6,7,8,9]  ,  np.float64)
x = np.array(  np.arange(20)  ,  np.float64)
dx= np.zeros_like(x) + 0.1

scattery = chi * np.random.normal(0,sigma, size=len(x))

y  = -0.02*x**2 + 2*x + scattery
dy = np.zeros_like(x) + sigma
#np.random.normal(5*sigma, sigma, size=len(x))

print( type(x) )     # numpy.ndarray
print( type(x[0] ) ) # goal is   numpy.float64
print( x.shape )
# print( x )
g = ROOT.TGraphErrors( len(x) , x.flatten("C"), y.flatten("C"), dx.flatten("C"), dy.flatten("C") )

# plot nicely on (maybe new) TCanvas
g.Draw("PAW")
ROOT.gPad.SetGrid()

# make it available from CLING
# 1st remove all with the same name
rmo = ROOT.gDirectory.Get(NAME)
while rmo!=None:
    print(f"x... removing previous object named {NAME}")
    ROOT.gDirectory.Remove(rmo)
    rmo = ROOT.gDirectory.Get(NAME)

# make it available from CLING
g.SetName("g")
g.SetTitle("Test Graph")
ROOT.gDirectory.Add(g)
ROOT.gDirectory.ls("g")


# ------------------------- MINUIT PART ----------------
from iminuit import cost, Minuit
import iminuit
from numba_stats import norm, uniform # faster replacements for scipy.stats functions


# --------------chi2
def model_chi2(x,   a,b,c):
    # print(xe)
    f = a* x**2 + b*x + c
    return f

# ---- for histograms, use cx...
c2 = cost.LeastSquares(x, y, dy, model_chi2)
print("=====================least squares====")

m2 = Minuit(c2,
            a=0.01, b=1, c=0.1 )
# m2.limits["a", "b", "c"] = (0, None)
m2.migrad()
#print(m2.errors) # error view
#print(m2.values) # value view


# print(iminuit.util.FMin() ) # ????
#print(m2.covariance) # NICE but NOT NEEDED matrix
print(m2.fmin)   #NICE table
print(m2.params) # NICE table

yf = model_chi2( x, m2.values['a'],
                 m2.values['b'],
                 m2.values['c'] )
#print(yf)
gf = ROOT.TGraph( len(x) , x.flatten("C"), yf.flatten("C") )
gf.SetLineColor(2)
gf.Draw("sameL")
