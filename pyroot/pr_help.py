#!/usr/bin/env python3

#
#  this also serves as an example of the framework:
#
#  1. run from CLING (CINT) using prun("help")
#  2. debug from commandline
#  3. this level of module is possible to edit and stay in CLING!
#  4. even if crashes, it is possible to stay in CLING

import ROOT

import prun   # this seems like mutual import, but it is not really
#      prun.register() .....


from fire import Fire


def main( *args ):
    print("_____________________HELP________________ BEGIN")
    if len(args)>0:
         print(f"i... arguments sent to help {args}")

    print("""
Above, there is a list of all modules available to this system.

    1. pr_help:    this file
    2. pr_load:    loads TGraphErrors or TGraph;
                   ARGS: filename xycolnamelist
                   E.G.: data1.txt y,_,x,dx,dy
                     order of (x..dy) corresponds to the column order
                     column names can be defined in the data file too:
                     with like #COLNAME: frame,hor,vert
                          (better dont use x,y,dx,dy to avoid confusion)
                     and loaded as x,y  #LOAD_AS: _,y,x
                            (this refers only to column position)
    3. pr_fit :    FIT the object (TGraph/Errors)
                   ARGS: object_name (automatically file without ext.)
                   E.G.: fit data1

NB:  The modules can be also run directly from commandline
E.G.: ./pr_load.py data1.txt x,y
""")
    print("_____________________HELP________________ END")
# Examples:
#     .x prun.C("help")
#     .x prun.C("load tracked.dat  x,y,no1,no2")
#     .x prun.C("fit tracked")


if __name__=="__main__":
    Fire(main)
    input('press ENTER to end...')
