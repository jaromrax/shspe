import ROOT
import glob
import os

#print("P... in callpy.py")

def listpy():
    #print("P... in fun listpy")
    lin = ROOT.gROOT.GetListOfSpecials().FindObject("pr_input")

    print(f"P... infput function found - see: {lin}")
    # lin.Print()
    inname = lin.GetName()
    intit = lin.GetTitle()
    print(f"P...    title={intit},  name={inname} " )


    files = glob.glob("pr_*.py")
    if len(files)==0:
        print("X... NO python FILES")
        return

    for ffile in files:
        print(f" ...    searching  /{intit}/ in {ffile}")
        if intit==ffile.lstrip("pr_").rstrip(".py"):
            print(f"P... got {ffile}")

            # ----- this way I get things back to C++-----------
            item = ROOT.TText( 0.01,0.01, ffile ) # Title == it
            item.SetName("pr_output")
            los = ROOT.gROOT.GetListOfSpecials()
            los.Add(item)
            return
    print(f"X... no file like pr_{intit}.py found")
    return

    # files = [ os.path.splitext(x)[0] for x in  files]
    print(files)
    #lr = ROOT.TList()
    #lr.SetName("pr_output")

    # This will create a list of all gobbed files.
    for it in files:
        #print(f" adding: {it}")
        item = ROOT.TText( 0.01,0.01, it )
        #print(item)
        #item.Print()
        #print(f" adding: {item}")

        lr.Add( item )
        # lr.Print()

    #print("P... TList finished")
    #lr.Print()
    #print("")
    # los.Print()
    # root [14] gROOT->GetListOfSpecials()->FindObject("pyreturn")->ls()
    return lr

#listpy()
